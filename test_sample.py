import pytest
from selenium.common.exceptions import NoSuchElementException


def test_incorrect_login(selenium, url, username, password):
    selenium.get(url)
    lusername = selenium.find_element_by_class_name("js-username-field")
    lpassword = selenium.find_element_by_class_name("js-password-field")

    lusername.send_keys(username + "1")
    lpassword.send_keys(password)

    try:
        selenium.find_element_by_class_name("EdgeButtom--medium").click()
        errmessage = selenium.find_element_by_class_name("message-text").text
        if(not errmessage):
            pytest.fail("not configured: %s" % (errmessage))
        else:
            pass

    except NoSuchElementException:
        selenium.save_screenshot("exception_test_incorrect_login.png")
        pytest.fail("Exception!")


def test_incorrect_password(selenium, url, username, password):
    selenium.get(url)
    _username = selenium.find_element_by_class_name("js-username-field")
    _password = selenium.find_element_by_class_name("js-password-field")
    _username.send_keys(username)
    _password.send_keys(password + "1")

    try:
        selenium.find_element_by_class_name("EdgeButtom--medium").click()
        errmessage = selenium.find_element_by_class_name("message-text").text
        if(not errmessage):
            pytest.fail("not configured: %s" % (errmessage))
        else:
            pass

    except NoSuchElementException:
        selenium.save_screenshot("exception_test_incorrect_password.png")
        pytest.fail("Exception!")


def test_login(selenium, url, username, password):
    selenium.get(url)
    _username = selenium.find_element_by_class_name("js-username-field")
    _password = selenium.find_element_by_class_name("js-password-field")

    _username.send_keys(username)
    _password.send_keys(password)

    try:
        selenium.find_element_by_class_name("EdgeButtom--medium").click()
        errmessage = selenium.find_element_by_class_name("stream").text
        if(not errmessage):
            pytest.fail("not configured: %s" % (errmessage))
        else:
            pass

    except NoSuchElementException:
        selenium.save_screenshot("exception_test_login.png")
        pytest.fail("Exception!")
