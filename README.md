# pytest-twitter

## Test scope:
Checking twitter login system.

**Test cases**
* incorrect login, correct password
* correct login, incorrect password
* correct login and password

## Getting Started

### Prerequisites

* Python 3 with PIP
* [Chromedriver](https://sites.google.com/a/chromium.org/chromedriver/downloads)

### Installing

- Clone this repo:
```git clone https://gitlab.com/spo0lsh/pytest-twitter.git```

- Install requirements module:
```pip3 -r requirements.txt```

### Configure
In ```conftest.py ```:
* path to google-chrome (default: system)
* install extension
* run in incognito mode
* run in headless mode

More information [here](http://pytest-selenium.readthedocs.io/en/latest/user_guide.html#chrome)

### Execute test

Go to pytest-twitter directory and execute pytest with options:
* --driver Chrome
* --html=path/to/report/directory
* --login=login
* --password=password

**Example**
- Linux:
```
~/.local/bin/pytest --driver Chrome --driver-path ./chromedriver --html=report/report_localhost.html --username=login@domain.tld --password="Password"
```

- Windows:
```
c:\Users\test\AppData\Local\Programs\Python\Python36-32\Scripts\py.test --driver Chrome --html=report/report_localhost.html --username=login@domain.tld --password="Password"
```

## Authors
* **Krzysztof Krych**

## Know issue
* Some time dependies - need add [Waits](http://selenium-python.readthedocs.io/waits.html) like **implicitly_wait**
