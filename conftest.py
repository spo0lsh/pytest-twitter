import pytest


@pytest.fixture
def chrome_options(chrome_options):
    # chrome_options.binary_location = '/usr/bin/google-chrome'
    # chrome_options.add_extension('/path/to/extension.crx')
    # chrome_options.add_argument('--incognito')
    # chrome_options.add_argument('--headless')
    return chrome_options


def pytest_addoption(parser):
    parser.addoption("--username", action="store", default="manager",
                     help="username")
    parser.addoption("--password", action="store", default="test",
                     help="password")
    parser.addoption("--url", action="store",
                     default="https://twitter.com/login", help="URL")


@pytest.fixture(scope="module")
def username(request):
    return request.config.getoption("--username")


@pytest.fixture(scope="module")
def password(request):
    return request.config.getoption("--password")


@pytest.fixture(scope="module")
def url(request):
    return request.config.getoption("--url")
